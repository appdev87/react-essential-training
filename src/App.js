import React, { useState, useEffect } from "react";
import './App.css';
import './logo.svg'
import { Routes, Route } from 'react-router-dom';
import { Menu, About, Events, Contact, Whoops404, Services, CompanyHistory, Location } from './pages';
import { Home } from './home/Home.js';
import { Login } from './login/Login.js';

// https://api.github.com/users/appdev87

function Navigation() {
	return (
		<div>
			<Menu />
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/about" element={<About />}>
					<Route path="services" element={<Services />} />
					<Route path="history" element={<CompanyHistory />} />
					<Route path="location" element={<Location />} />

				</Route >
				<Route path="/events" element={<Events />} />
				<Route path="/contact" element={<Contact />} />
				<Route path="/login" element={<Login username="kate"/>} />
				<Route path="*" element={<Whoops404 />} />
			</Routes>

		</div>
	);
}

function Footer() {
	return (
		<div class="footer">
			<h1>This is the footer</h1>
		</div>
	);
}

function App() {
	return (
		<>
			<Navigation />
			<Footer />
		</>
	)
}

export default App;
