import React from 'react';
import { Link, useLocation, Outlet } from 'react-router-dom';



export function Menu() {
	return (
		<>
			<nav>
				<Link to="/">Home</Link>
				<Link to="/about">About</Link>
				<Link to="/events">Events</Link>
				<Link to="/contact">Contact</Link>
				<Link to="/login">Login</Link>
			</nav>
		</>
	);
}

// export function Home() {
// 	return (
// 		<>
// 			<h1>[Company Website]</h1>
// 		</>
// 	);
// }

export function About() {
	return (
		<>
			<h1>[About]</h1>
			<Outlet />
		</>
	);
}

export function Events() {
	return (
		<>
			<h1>[Events]</h1>
		</>
	);
}

export function Contact() {
	return (
		<>
			<h1>[Contact]</h1>
		</>
	);
}

export function Services() {
	return (
		<>
			<h1>Our Services</h1>
		</>
	);
}
export function CompanyHistory() {
	return (
		<>
			<h1>Our Company History</h1>
		</>
	);
}
export function Location() {
	return (
		<>
			<h1>Our Location</h1>
		</>
	);
}

export function Whoops404() {
	let location = useLocation();

	return (
		<div>
			<h1>Hmmm... Seems that you are lost!!!</h1>
			<h2>The page {location.pathname} does not exist!!!</h2>
			<Link to="/">Go Back Home</Link>
		</div>
	);
}