import React from 'react';
import { useFormik } from 'formik';

import './Login.css';


export function Login({ username }) {
    if (username === "jane") {
        return <SignIn username={username} />;
    } else {
        return <SignUp username={username} />;
    }
}

function SignIn({ username }) {
    return (
        <div>
            <h1>Here is the signIn sheet: {username}</h1>
        </div>
    )
}

function SignUp({ username }) {

    const formik = useFormik({

        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
        },
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        },
    });

    return (
        <div>
            <h1>Your name could not be found {username}</h1>
            <h2>Would you like to sign up into our accounts?</h2>
            <form onSubmit={formik.handleSubmit}>

                <label htmlFor="firstName">First Name</label>

                <input
                    id="firstName"
                    name="firstName"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.firstName}
                />

                <label htmlFor="lastName">Last Name</label>

                <input
                    id="lastName"
                    name="lastName"
                    type="text"
                    onChange={formik.handleChange}
                    value={formik.values.lastName}
                />



                <label htmlFor="email">Email Address</label>
                <input
                    id="email"
                    name="email"
                    type="email"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                />

                <button type="submit">Submit</button>
            </form>
        </div>
    )
}